//
//  ViewController.swift
//  AppGui
//
//  Created by Guilherme Borba on 04/02/21.
//

import UIKit

class ViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {

    @IBOutlet weak var imagemCalcas: UIImageView!
    @IBOutlet weak var marcasCalcas: UISegmentedControl!
    @IBOutlet weak var estiloCalcas: UIPickerView!
    @IBOutlet weak var valorLabel: UILabel!
    @IBOutlet weak var valorReal: UILabel!
    
    var tagBtns = 1
    
    
    var dataCalcas = ["Cintura baixa","Cintura Média","Cintura Alta","Skinny"]
    
    let imageDataCalcas = [UIImage(named: "couro@4x")!,UIImage(named: "jogger@4x")!,UIImage(named: "legging@4x")!,UIImage(named: "pantacourt@4x")!,UIImage(named: "flare@4x")!,UIImage(named: "sarja@4x")!,UIImage(named: "social@4x")!,UIImage(named: "jeans@4x")!]
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return dataCalcas.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return dataCalcas[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView
    {
        
        
        let pickerLabel = UILabel()
        
        pickerLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        pickerLabel.text = dataCalcas[row]
        //pickerLabel.backgroundColor = #colorLiteral(red: 0.8602875471, green: 0.8603077531, blue: 0.860296905, alpha: 1)
        pickerLabel.font = UIFont(name: pickerLabel.font.fontName, size: 25)
        pickerLabel.font = UIFont(name: "Montserrat-Regular", size: 12) // In this use your custom font
        pickerLabel.textAlignment = NSTextAlignment.center
        return pickerLabel
        
        
    }
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        
    }
    
    
    @IBAction func estiloCalcas(_ sender: UIButton) {
        
        
        self.tagBtns = sender.tag

        switch tagBtns {
        case 1:
            let imgCalcas = imageDataCalcas[0]
            UIView.transition(with: self.imagemCalcas,
                              duration: 0.5,
                              options: .transitionCrossDissolve,
                              animations: { self.imagemCalcas.image = imgCalcas },
                              completion: nil)
            self.valorLabel.text = sender.title(for: .normal)
            if marcasCalcas.selectedSegmentIndex == 0 && (estiloCalcas.selectedRow(inComponent: 0) == 0){
                
                valorReal.text = "2.000,00"
            } else if marcasCalcas.selectedSegmentIndex == 1 && (estiloCalcas.selectedRow(inComponent: 0) == 0) {
                    valorReal.text = "2.500,00"
                
            } else if marcasCalcas.selectedSegmentIndex == 2 && (estiloCalcas.selectedRow(inComponent: 0) == 0) {
                    valorReal.text = "3.500,00"
            
        } else if marcasCalcas.selectedSegmentIndex == 0 && (estiloCalcas.selectedRow(inComponent: 0) == 1) {
                valorReal.text = "7.137,00"
        
            
        } else if marcasCalcas.selectedSegmentIndex == 1 && (estiloCalcas.selectedRow(inComponent: 0) == 1) {
                valorReal.text = "8.137,00"
        
            
        } else if marcasCalcas.selectedSegmentIndex == 2 && (estiloCalcas.selectedRow(inComponent: 0) == 1) {
                valorReal.text = "9.444,00"
        
        }
            
            
        
        case 2:
            let imgCalcas = imageDataCalcas[1]
            UIView.transition(with: self.imagemCalcas,
                              duration: 0.5,
                              options: .transitionCrossDissolve,
                              animations: { self.imagemCalcas.image = imgCalcas },
                              completion: nil)
            self.valorLabel.text = sender.title(for: .normal)
            if marcasCalcas.selectedSegmentIndex == 2 && (estiloCalcas.selectedRow(inComponent: 0) == 0){
                
                valorReal.text = "1.800,00"
            } else if marcasCalcas.selectedSegmentIndex == 1 && (estiloCalcas.selectedRow(inComponent: 0) == 0) {
                    valorReal.text = "1.300,00"
                
            } else if marcasCalcas.selectedSegmentIndex == 0 && (estiloCalcas.selectedRow(inComponent: 0) == 0) {
                    valorReal.text = "3.100,00"
                
                
            }
        case 3:
            let imgCalcas = imageDataCalcas[2]
            UIView.transition(with: self.imagemCalcas,
                              duration: 0.5,
                              options: .transitionCrossDissolve,
                              animations: { self.imagemCalcas.image = imgCalcas },
                              completion: nil)
            self.valorLabel.text = sender.title(for: .normal)
        case 4:
            let imgCalcas = imageDataCalcas[3]
            UIView.transition(with: self.imagemCalcas,
                              duration: 0.5,
                              options: .transitionCrossDissolve,
                              animations: { self.imagemCalcas.image = imgCalcas },
                              completion: nil)
            self.valorLabel.text = sender.title(for: .normal)
        case 5:
            let imgCalcas = imageDataCalcas[4]
            UIView.transition(with: self.imagemCalcas,
                              duration: 0.5,
                              options: .transitionCrossDissolve,
                              animations: { self.imagemCalcas.image = imgCalcas },
                              completion: nil)
            self.valorLabel.text = sender.title(for: .normal)
        case 6:
            let imgCalcas = imageDataCalcas[5]
            UIView.transition(with: self.imagemCalcas,
                              duration: 0.5,
                              options: .transitionCrossDissolve,
                              animations: { self.imagemCalcas.image = imgCalcas },
                              completion: nil)
            self.valorLabel.text = sender.title(for: .normal)
        case 7:
            let imgCalcas = imageDataCalcas[6]
            UIView.transition(with: self.imagemCalcas,
                              duration: 0.5,
                              options: .transitionCrossDissolve,
                              animations: { self.imagemCalcas.image = imgCalcas },
                              completion: nil)
            self.valorLabel.text = sender.title(for: .normal)
        case 8:
            let imgCalcas = imageDataCalcas[7]
            UIView.transition(with: self.imagemCalcas,
                              duration: 0.5,
                              options: .transitionCrossDissolve,
                              animations: { self.imagemCalcas.image = imgCalcas },
                              completion: nil)
            self.valorLabel.text = sender.title(for: .normal)
            
        default:
            break
        }
    }
    
}

